import java.util.Arrays;

public class Matrix implements IMatrix{
    protected double[] coeffs;
    protected double determinate;
    protected boolean flag;
    protected int dim;

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Почему вам не нравятся матрицы 0 размера? Это же лучше чем null.
    */
    // Москвичёв М.М. 20.06.2020; Ну, думал так лучш, но исправил)
    public Matrix(int n) throws MatrixException{
        if (n < 0)
            throw new MatrixException(MatrixErrorCode.WRONG_SIZE);
        coeffs = new double[n*n];
        dim = n;
        /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
         Создаваемые массивы заполняются значением по умолчанию. Для чисел это 0.
         Поэтому цикл можно выкинуть.
        */
        // Москвичёв М.М. 20.06.2020; Сделано.
    }

    //m,n - строки и столбцы соответственно
    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Что помешало вам сохранить размер матрицы? Чтобы не считать квадратный корень, который считается не очень быстро.
    */
    // Москвичёв М.М. 20.06.2020; Исправлено.
    @Override
    public double getElem(int m, int n) throws MatrixException{
        if ((n < 0) || (n > dim-1) || (m < 0) || (m > dim-1))
            throw new MatrixException(MatrixErrorCode.WRONG_SIZE);
        return coeffs[dim*m + n];
    }

    @Override
    public void changeElem(int m, int n, double change) throws MatrixException {
        if ((n < 0) || (n > dim-1) || (m < 0) || (m > dim-1))
            throw new MatrixException(MatrixErrorCode.WRONG_INDEXES);
        coeffs[dim*m + n] = change;
        flag = false;
        /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
         Не пишете this без необходимости - это не добавляет читаемости, а загромождает код.
          Идея подсвечивает локальные переменные и поля разным цветом.
        */
        // Москвичёв М.М. 20.06.2020; Хорошо, исправил.
    }

    @Override
    public double calculateDeterminate() throws MatrixException{
        /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
         Вот если начать читать программу тут, то может показаться, что за if есть что-то или есть else...
         а там только return.

         В таких случаях пишем

         if(flag) {
            return determinate;
         }

         а потом тело if без отступа - это сильно улучшает читаемость кода.
        */
        // Москвичёв М.М. 20.06.2020; Хорошо, исправил.
        if (flag) {
            return determinate;
        }
        else {
            /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
             это читерство сохранить в переменную размер матрицы )
             нужно верить в компилятор, который обязан отоптимизировать все вызовы вычисления квадратного корня!
            */
            // Москвичёв М.М. 20.06.2020; Исправил.
            double result = 1;

            Matrix copyMatrix = new Matrix(dim);
            copyMatrix.coeffs = Arrays.copyOf(coeffs, coeffs.length);
            /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
             можно было проще. Вы можете обращаться к приватным полям другого объекта этого класса.
            copyMatrix.coeffs = Arrays.copyOf(coeffs, coeffs.length);
            */
            //Москвичёв М.М. 20.06.2020; Хорошо, исправил.
            for (int i=0; i<dim-1; i++){
                if(Math.abs(copyMatrix.getElem(i,i)) < 1E-9)
                {
                    for (int j = i+1; j < dim; j++) {
                        if(Math.abs(copyMatrix.getElem(j,i)) > 1E-9){
                            for (int k = 0; k < dim; k++) {
                                double temp = copyMatrix.getElem(j,k);
                                copyMatrix.changeElem(j,k,copyMatrix.getElem(i,k));
                                copyMatrix.changeElem(i,k,temp);
                            }
                            result *= -1;
                            break;
                        }
                        else {
                            setDeterminate(0);
                            return determinate;
                        }
                    }
                }
                for (int j=i+1; j<dim; j++){
                    double coef = copyMatrix.getElem(j, i) / copyMatrix.getElem(i, i);
                    for (int k=0; k<dim; k++){
                        copyMatrix.changeElem(j, k, copyMatrix.getElem(j, k)-((copyMatrix.getElem(i, k)*coef)));
                    }
                }
                result *= copyMatrix.getElem(i, i);
            }

            result *= copyMatrix.getElem(dim -1, dim - 1);

            flag = true;
            setDeterminate(result);
        }
        return determinate;
    }

    public double getDeterminate() {
        return determinate;
    }

    public void setDeterminate(double determinate) {
        this.determinate = determinate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix)) return false;
        Matrix that = (Matrix) o;
        if (coeffs.length != that.coeffs.length)
            return false;
        for (int i = 0; i<Math.sqrt(coeffs.length); i++)
            for (int j = 0; j<Math.sqrt(coeffs.length); j++) {
                try {
                    if ((Math.abs(getElem(i,j)-that.getElem(i,j)))>1e-9)
                        return false;
                } catch (MatrixException e) {
                    return false;
                }
            }
        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coeffs);
    }
}
