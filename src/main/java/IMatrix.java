public interface IMatrix {
    double getElem(int m, int n) throws MatrixException;
    void changeElem(int m, int n, double change) throws MatrixException;
    double calculateDeterminate() throws MatrixException;
}
