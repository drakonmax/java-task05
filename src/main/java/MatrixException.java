
/* Филиппов А.В. 20.06.2020 Комментарий не удалять.
 Не наследуйте RuntimeException. Это исключения JVM.
 Ваши исключения должны наследовать Exception.
*/
// Москвичёв М.М. 20.06.2020; Сделано.
public class MatrixException extends Exception {
    private final MatrixErrorCode errorCode;


    public MatrixException(MatrixErrorCode errorCode) {
        this.errorCode = errorCode;
    }


    public MatrixErrorCode getErrorCode() {
        return errorCode;
    }
}
