public enum MatrixErrorCode {
    WRONG_SIZE("This size is wrong!"),
    WRONG_INDEXES("This indexes are wrong!");

    private final String errorString;

    MatrixErrorCode(String errorString){this.errorString = errorString;}
    public String getErrorString() {
        return errorString;
    }

}
