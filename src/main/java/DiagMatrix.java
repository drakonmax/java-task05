public class DiagMatrix extends Matrix {

    public DiagMatrix(int n) throws MatrixException  {
        super(n);
    }

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Вне диагонали ставить 0 можно - никаких исключений.
    */
    // Москвичёв М.М. 20.06.2020; Хорошо, исправил.
    @Override
    public void changeElem(int m, int n, double change)  throws MatrixException{
    if (m!=n && change!=0) throw new MatrixException(MatrixErrorCode.WRONG_INDEXES);
        super.changeElem(m, n, change);
    }

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Бесполезный метод )
    */
    @Override
    public double calculateDeterminate() throws MatrixException{
        return super.calculateDeterminate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix)) return false;
        Matrix that = (Matrix) o;
        if (coeffs.length != that.coeffs.length)
            return false;
        for (int i = 0; i<Math.sqrt(coeffs.length); i++) {
            try {
                if ((Math.abs(getElem(i,i)-that.getElem(i,i)))>1e-9)
                    return false;
            } catch (MatrixException e) {
                return false;
            }
        }
        return true;
    }
}
