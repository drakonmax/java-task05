import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestUpTriangleMatrix {
    @Test
    public void testUpTriangleMatrix()
    {
        try {
            Matrix matrix1 = new UpTriangleMatrix(-1);
            fail();
        }
        catch(MatrixException e){
            assertEquals(MatrixErrorCode.WRONG_SIZE, e.getErrorCode());
        }

    }
    @Test
    public void testUpTriangleMatrixGetSet() throws MatrixException
    {
        Matrix matrix1 = new UpTriangleMatrix(5);
        Matrix matrix2 = new UpTriangleMatrix(5);
        matrix2.changeElem(0,3,5.7);

        assertEquals(0,matrix1.getElem(0,3));
        assertEquals(5.7,matrix2.getElem(0,3));
        try {
            matrix2.changeElem(2,0,5.7);
            fail();
        }
        catch (MatrixException e){
            assertEquals(MatrixErrorCode.WRONG_INDEXES, e.getErrorCode());
        }
    }
    @Test
    public void testUpTriangleMatrixDeterminate() throws MatrixException
    {
        Matrix matrix1 = new UpTriangleMatrix(3);
        assertEquals(false, matrix1.flag);
        matrix1.changeElem(0,0,3.7);
        matrix1.changeElem(0,1,6.2);
        matrix1.changeElem(0,2,2.1);
        matrix1.changeElem(1,1,5.8);
        matrix1.changeElem(1,2,4.3);
        matrix1.changeElem(2,2,2.1);
        matrix1.calculateDeterminate();
        assertEquals(45.066, matrix1.determinate, 1e-9);
        assertEquals(true, matrix1.flag);
        matrix1.changeElem(0,0,3.6);
        assertEquals(false, matrix1.flag);
        matrix1.changeElem(0,0,0);
        matrix1.calculateDeterminate();
        assertEquals(0, matrix1.determinate);
    }
    @Test
    public void testUpTriangleMatrixEquals() throws MatrixException
    {
        Matrix matrix1 = new UpTriangleMatrix(3);
        matrix1.changeElem(0,0,3.7);
        matrix1.changeElem(0,1,6.2);
        matrix1.changeElem(0,2,2.1);
        matrix1.changeElem(1,1,5.8);
        matrix1.changeElem(1,2,4.3);
        matrix1.changeElem(2,2,2.1);
        Matrix matrix2 = new UpTriangleMatrix(3);
        matrix2.changeElem(0,0,3.7);
        matrix2.changeElem(0,1,6.2);
        matrix2.changeElem(0,2,2.1);
        matrix2.changeElem(1,1,5.8);
        matrix2.changeElem(1,2,4.3);
        matrix2.changeElem(2,2,2.1);
        assertEquals(true, matrix1.equals(matrix2));
    }
}
